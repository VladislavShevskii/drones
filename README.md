# Drones

# Api List
[POST] /register, /load/{droneId}, /complete_load/{droneId}, <br />
/start/{droneId}, /charge/{droneId}, /check_battery/{droneId}<br /><br />
[GET] /check_all_drones, /check_load_available_drones

# Api descripiton
/register - 					register a drone using json body<br /><br />
/load/{droneId} -				add medication for a registered drone<br /><br />
/complete_load/{droneId} -		complete load medications (changing state LOADING->LOADED, drones with state LOADING cannot be started)<br /><br />
/start/{droneId} - 				asynchronously run a drone to go specified meters to target (meters are in json body)<br /><br />
/charge/{droneId} - 			charge a low battery drone (only state IDLE is valid for drone to be charged)<br /><br />
/check_battery/{droneId} - 		get battery level for a drone<br />

/check_all_drones - 			get all registered drones<br /><br />
/check_load_available_drones - 	get all available for loading drones

# Steps to run 2 loaded drones
1. localhost:8080/register
json:
{
			"serialNumber": "a",
			"model": "LIGHTWEIGHT",
			"weightLimit": 300,
		}
2. localhost:8080/register
json:
{
			"serialNumber": "b",
			"model": "MIDDLEWEIGHT",
			"weightLimit": 400,
		}
3. localhost:8080/load/1
json:
{
			"name": "medication1",
			"weight": 100,
			"code": "CODE_1",
			"image": "http://..."
		}
// value '1' after load can be different, see step 1 result to get right drone id<br />
3. localhost:8080/load/2
json:
{
			"name": "medication12",
			"weight": 200,
			"code": "CODE_2",
			"image": "http://..."
		}
// value '2' after load can be different, see step 2 result to get right drone id<br />
4. localhost:8080/complete_load/1
// value '1' after complete_load can be different, see step 1 result to get right drone id<br />
5. localhost:8080/complete_load/2
// value '2' after complete_load can be different, see step 2 result to get right drone id<br />
6. localhost:8080/start/1
json: 
{
	"distance": 350
}
// value '1' after start can be different, see step 1 result to get right drone id<br />
7. localhost:8080/start/2
json: 
{
	"distance": 350
}
// value '2' after start can be different, see step 2 result to get right drone id<br />

# How to check drones states
You can use whether /check_all_drones, /check_load_available_drones, /check_battery/{droneId}, <br />
or You can see periodic task output in the project console, output example:<br />
2023-01-18T18:04:58.019850	Checking drones:
--------------------------------------------------------
Drone with id '1' has battery 100%
Drone with id '2' has battery 100%
--------------------------------------------------------

# How to build and run
1. Install java
2. Add java ot path environment variable
3. Install maven
4. Add maven to path environment variable
5. In terminal: 
 5.1. go to project root 
 5.2. execute: "mvn clean install"
 5.3. go to target directory
 5.4. execute: "java -jar project-1.0-SNAPSHOT.jar"
