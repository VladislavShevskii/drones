package com.vshevskii.scheduling;

import com.vshevskii.dto.DroneDto;
import com.vshevskii.dto.DroneWithMedicationDto;
import com.vshevskii.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.vshevskii.messages.LoggingMessages.CHECKING_BATTERY;
import static com.vshevskii.messages.LoggingMessages.LOGGING_SPLITTER;

@Service
@RequiredArgsConstructor
public class Scheduler {

    private final DroneService droneService;

    @Scheduled(cron = "*/2 * * * * *")
    public void checkDronesBatteryLevel() {
        List<DroneWithMedicationDto> dronesWithMedications = droneService.findAll();
        StringBuilder sb = new StringBuilder();
        sb.append(LocalDateTime.now() + "\tChecking drones:\n");
        sb.append(LOGGING_SPLITTER);
        for (DroneWithMedicationDto droneWithMedications : dronesWithMedications) {
            DroneDto drone = droneWithMedications.getDrone();
            sb.append(String.format(CHECKING_BATTERY, drone.getId(), drone.getBattery()) + "\n");
        }
        sb.append(LOGGING_SPLITTER);
        System.out.print(sb.toString());
    }
}
