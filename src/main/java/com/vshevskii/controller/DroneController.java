package com.vshevskii.controller;

import com.vshevskii.dto.*;
import com.vshevskii.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class DroneController {

    private final DroneService droneService;


    @PostMapping("/register")
    public DroneDto register(@RequestBody @Valid DroneDto drone) {
        return droneService.initDrone(drone);
    }

    @PostMapping("/load/{droneId}")
    public DroneWithMedicationShortDto loadDroneWithMedication(@PathVariable Integer droneId,
                                                               @RequestBody @Valid MedicationDto medication) {
        return droneService.loadDroneWithMedication(droneId, medication);
    }

    @PostMapping("complete_load/{droneId}")
    public DroneWithMedicationShortDto completeLoad(@PathVariable Integer droneId) {
        return droneService.completeLoad(droneId);
    }

    @PostMapping("/start/{droneId}")
    public void startDrone(@PathVariable Integer droneId, @RequestBody StartArgumentsDto startArgumentsDto) {
        droneService.startDrone(droneId, startArgumentsDto.getDistance());
    }

    @GetMapping("/check_all_drones")
    public List<DroneWithMedicationDto> checkAllDrones() {
        return droneService.findAll();
    }

    @GetMapping("/check_load_available_drones")
    public List<DroneWithMedicationDto> checkAvailableToLoadDrones() {
        return droneService.findAvailableToLoadDrones();
    }

    @PostMapping("/charge/{droneId}")
    public DroneDto chargeDrone(@PathVariable Integer droneId) {
        return droneService.chargeDrone(droneId);
    }

    @GetMapping("/check_battery/{droneId}")
    public Integer checkBattery(@PathVariable Integer droneId) {
        DroneDto drone = droneService.findById(droneId);
        return drone.getBattery();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleIllegalArgumentException(IllegalArgumentException e) {
        return e.getMessage();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return e.getMessage();
    }
}
