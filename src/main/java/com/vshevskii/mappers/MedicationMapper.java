package com.vshevskii.mappers;

import com.vshevskii.dao.entities.Medication;
import com.vshevskii.dto.MedicationDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicationMapper {

    MedicationDto toDto(Medication entity);

    Medication toEntity(MedicationDto dto);
}
