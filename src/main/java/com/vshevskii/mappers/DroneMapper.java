package com.vshevskii.mappers;

import com.vshevskii.dao.entities.Drone;
import com.vshevskii.dto.DroneDto;
import com.vshevskii.dto.DroneWithMedicationDto;
import com.vshevskii.dto.DroneWithMedicationShortDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DroneMapper {

    DroneDto toDto(Drone entity);

    Drone toEntity(DroneDto dto);

    DroneWithMedicationShortDto toDroneWithMedicationShort(Drone entity);

    @Mapping(target = "drone", source = "entity")
    DroneWithMedicationDto toDroneWithMedication(Drone entity);
}
