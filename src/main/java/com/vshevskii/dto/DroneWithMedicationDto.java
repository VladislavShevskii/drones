package com.vshevskii.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneWithMedicationDto {

    private DroneDto drone;

    private List<MedicationDto> medications;
}
