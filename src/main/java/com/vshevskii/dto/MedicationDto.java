package com.vshevskii.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicationDto {

    private int id;

    @Pattern(regexp = "^[a-zA-Z0-9_-]*$", message = "Name is not valid - allowed only letters, numbers, ‘-‘, ‘_’")
    private String name;

    @Max(value = 500, message = "Weight is not valid - 500gr max")
    @Min(value = 0, message = "Weight is not valid - 0gr min")
    private int weight;

    @Pattern(regexp = "^[A-Z0-9_-]*$", message = "Code is not valid - allowed only upper case letters, underscore and numbers")
    private String code;

    private String image;
}
