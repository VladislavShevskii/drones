package com.vshevskii.dto;

import com.vshevskii.dao.enums.ModelType;
import com.vshevskii.dao.enums.StateType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneDto {

    private int id;

    private String serialNumber;

    private ModelType model;

    @Max(value = 500, message = "Weight is not valid - 500gr max")
    @Min(value = 0, message = "Weight is not valid - 0gr min")
    private int weightLimit;

    @Max(value = 100, message = "Battery is not valid - 100% max")
    @Min(value = 0, message = "Battery is not valid - 0% min")
    private int battery;

    private StateType state;
}
