package com.vshevskii.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneWithMedicationShortDto {

    private int id;

    private List<MedicationDto> medications;
}
