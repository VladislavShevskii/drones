package com.vshevskii.service;

import com.vshevskii.dao.DroneRepository;
import com.vshevskii.dao.entities.Drone;
import com.vshevskii.dao.entities.Medication;
import com.vshevskii.dao.enums.StateType;
import com.vshevskii.dto.DroneDto;
import com.vshevskii.dto.DroneWithMedicationDto;
import com.vshevskii.dto.DroneWithMedicationShortDto;
import com.vshevskii.dto.MedicationDto;
import com.vshevskii.mappers.DroneMapper;
import com.vshevskii.mappers.MedicationMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.vshevskii.messages.ErrorMessages.*;

@Service
@RequiredArgsConstructor
@EnableAsync(proxyTargetClass = true)
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;

    private final DroneMapper droneMapper;

    private final MedicationMapper medicationMapper;

    private final MedicationService medicationService;

    private final int DISTANCE_PER_1_BATTERY_PERCENTAGE_COEFF = 2000;

    private final int WAITING_MILLIS_PER_1_BATTERY_PERCENTAGE = 100;

    private final int CRITICAL_BATTERY_PERCENTAGE = 0;

    @Override
    public DroneDto initDrone(DroneDto drone) {
        Drone entity = droneMapper.toEntity(drone);
        entity.setState(StateType.IDLE);
        entity.setBattery(100);
        Drone result = droneRepository.save(entity);
        return droneMapper.toDto(result);
    }

    @Override
    public DroneWithMedicationShortDto loadDroneWithMedication(Integer droneId, MedicationDto medication) {
        Drone storedDrone = findEntityById(droneId);
        checkIfCanBeLoaded(medication.getWeight(), storedDrone);
        medicationService.saveMedications(Collections.singletonList(medication));
        Medication medicationEntity = medicationMapper.toEntity(medication);
        if (CollectionUtils.isEmpty(storedDrone.getMedications())) {
            List<Medication> medications = new ArrayList<>();
            medications.add(medicationEntity);
            storedDrone.setMedications(medications);
        } else {
            storedDrone.getMedications().add(medicationEntity);
        }
        storedDrone.setState(StateType.LOADING);
        Drone updatedDrone = droneRepository.save(storedDrone);
        return droneMapper.toDroneWithMedicationShort(updatedDrone);
    }

    @Override
    public DroneWithMedicationShortDto completeLoad(Integer droneId) {
        Drone storedDrone = findEntityById(droneId);
        checkIfCanCompleteLoad(storedDrone.getState());
        storedDrone.setState(StateType.LOADED);
        storedDrone = droneRepository.save(storedDrone);
        return droneMapper.toDroneWithMedicationShort(storedDrone);
    }

    @Override
    @Async
    public void startDrone(int droneId, int distanceMeters) {
        Drone storedDrone = findEntityById(droneId);
        checkIfCanBeStarted(storedDrone);
        storedDrone.setState(StateType.DELIVERING);
        droneRepository.save(storedDrone);
        int totalWeight = storedDrone.getMedications().stream().mapToInt(Medication::getWeight).sum();
        int remainDistance = distanceMeters;
        int remainBattery = storedDrone.getBattery();
        while (remainBattery > CRITICAL_BATTERY_PERCENTAGE) {
            if (remainDistance <= 0) {
                returnDrone(storedDrone, distanceMeters - remainDistance, remainBattery, totalWeight);
                return;
            }
            remainBattery--;
            remainDistance -= DISTANCE_PER_1_BATTERY_PERCENTAGE_COEFF / (totalWeight + 1);
            try {
                Thread.sleep(WAITING_MILLIS_PER_1_BATTERY_PERCENTAGE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            storedDrone.setBattery(remainBattery);
            droneRepository.save(storedDrone);
        }
        if (remainDistance > 0) {
            System.err.println(String.format(DRONE_HAS_NOT_COMPLETED_FLIGHT, droneId, distanceMeters, totalWeight));
            return;
        } else {
            remainDistance = distanceMeters;
            totalWeight = 0;
        }
        returnDrone(storedDrone, remainDistance, remainBattery, totalWeight);
    }

    @Override
    public DroneDto chargeDrone(int droneId) {
        Drone storedDrone = findEntityById(droneId);
        checkIfCanBeCharged(storedDrone.getState());
        storedDrone.setBattery(100);
        storedDrone = droneRepository.save(storedDrone);
        return droneMapper.toDto(storedDrone);
    }

    @Override
    public List<DroneWithMedicationDto> findAll() {
        return droneRepository.findAll().stream().map(droneMapper::toDroneWithMedication).collect(Collectors.toList());
    }

    @Override
    public List<DroneWithMedicationDto> findAvailableToLoadDrones() {
        List<DroneWithMedicationDto> result = new ArrayList<>();
        List<DroneWithMedicationDto> all = findAll();
        for (DroneWithMedicationDto droneWithMedication : all) {
            try {
                checkIfCanBeLoaded(1, droneMapper.toEntity(droneWithMedication.getDrone()));
                result.add(droneWithMedication);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public DroneDto findById(Integer droneId) {
        return droneMapper.toDto(findEntityById(droneId));
    }

    private void checkWeightLimit(int newMedicationWeight, Drone drone) {
        int totalWeight = CollectionUtils.emptyIfNull(drone.getMedications()).stream().mapToInt(Medication::getWeight)
                .sum();
        if (newMedicationWeight + totalWeight > drone.getWeightLimit()) {
            throw new IllegalArgumentException(String.format(DRONE_WEIGHT_LIMIT_EXCEEDED, totalWeight,
                    drone.getWeightLimit()));
        }
    }

    private void checkIfCanBeLoaded(int newMedicationWeight, Drone drone) {
        checkWeightLimit(newMedicationWeight, drone);
        // check state
        if (drone.getState() != StateType.IDLE && drone.getState() != StateType.LOADING) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_STATE_FOR_LOAD, drone.getState()));
        }
        // check battery
        if (drone.getBattery() < 25) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_BATTERY_LEVEL_FOR_LOAD, drone.getBattery()));
        }
    }

    private void checkIfCanBeStarted(Drone drone) {
        if (drone.getState() != StateType.IDLE && drone.getState() != StateType.LOADED) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_STATE, drone.getState()));
        }
        // check battery
        if (drone.getBattery() <= 0) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_BATTERY_LEVEL, drone.getBattery()));
        }
    }

    private void checkIfCanCompleteLoad(StateType droneState) {
        if (droneState != StateType.LOADING) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_STATE_FOR_COMPLETE_LOAD, droneState));
        }
    }

    private void checkIfCanBeCharged(StateType droneState) {
        if (droneState != StateType.IDLE) {
            throw new IllegalArgumentException(String.format(DRONE_INVALID_STATE_FOR_CHARGING, droneState));
        }
    }

    private void freeMedicationsForDrone(Drone drone) {
        drone.setMedications(Collections.emptyList());
        droneRepository.save(drone);
    }

    private Drone findEntityById(Integer droneId) {
        Optional<Drone> storedDroneOpt = droneRepository.findById(droneId);
        if (storedDroneOpt.isEmpty()) {
            throw new IllegalArgumentException(String.format(DRONE_NOT_FOUND, droneId));
        }
        return storedDroneOpt.get();
    }


    private void returnDrone(Drone drone, Integer distanceMeters, Integer remainBattery, Integer totalWeight) {
        freeMedicationsForDrone(drone);
        drone.setState(StateType.RETURNING);
        droneRepository.save(drone);
        Integer remainDistance = distanceMeters;
        while (remainBattery > CRITICAL_BATTERY_PERCENTAGE) {
            if (remainDistance <= 0) {
                break;
            }
            remainBattery--;
            remainDistance -= DISTANCE_PER_1_BATTERY_PERCENTAGE_COEFF / (totalWeight + 1);
            try {
                Thread.sleep(WAITING_MILLIS_PER_1_BATTERY_PERCENTAGE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            drone.setBattery(remainBattery);
            droneRepository.save(drone);
        }
        if (remainDistance > 0) {
            System.err.println(String.format(DRONE_HAS_NOT_RETURNED, drone.getId()));
            return;
        }
        drone.setState(StateType.IDLE);
        droneRepository.save(drone);
    }
}
