package com.vshevskii.service;

import com.vshevskii.dto.DroneDto;
import com.vshevskii.dto.DroneWithMedicationDto;
import com.vshevskii.dto.DroneWithMedicationShortDto;
import com.vshevskii.dto.MedicationDto;

import java.util.List;

public interface DroneService {

    DroneDto initDrone(DroneDto drone);

    DroneWithMedicationShortDto loadDroneWithMedication(Integer droneId, MedicationDto medication);

    DroneWithMedicationShortDto completeLoad(Integer droneId);

    void startDrone(int droneId, int distanceMeters);

    DroneDto chargeDrone(int droneId);

    List<DroneWithMedicationDto> findAll();

    List<DroneWithMedicationDto> findAvailableToLoadDrones();

    DroneDto findById(Integer droneId);
}
