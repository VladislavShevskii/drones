package com.vshevskii.service;

import com.vshevskii.dto.MedicationDto;

import java.util.List;

public interface MedicationService {

    List<MedicationDto> saveMedications(List<MedicationDto> medications);
}
