package com.vshevskii.service;

import com.vshevskii.dao.MedicationRepository;
import com.vshevskii.dao.entities.Medication;
import com.vshevskii.dto.MedicationDto;
import com.vshevskii.mappers.MedicationMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.vshevskii.messages.ErrorMessages.CONTAINS_VIOLATIONS;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;

    private final ValidatorFactory validatorFactory;

    private final MedicationMapper medicationMapper;

    @Override
    public List<MedicationDto> saveMedications(List<MedicationDto> medications) {
        if (!checkIfMedicationsValidToSave(medications)) {
            throw new IllegalArgumentException(CONTAINS_VIOLATIONS);
        }
        List<Medication> medicationEntities = CollectionUtils.emptyIfNull(medications).stream()
                .map(medicationMapper::toEntity).collect(Collectors.toList());
        medicationEntities = medicationRepository.saveAll(medicationEntities);
        return medicationEntities.stream().map(medicationMapper::toDto).collect(Collectors.toList());
    }

    private boolean checkIfMedicationsValidToSave(List<MedicationDto> medications) {
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<MedicationDto>> violations = new HashSet<>();
        for (MedicationDto medication : medications) {
            violations.addAll(validator.validate(medication));
        }
        return CollectionUtils.isEmpty(violations);
    }
}
