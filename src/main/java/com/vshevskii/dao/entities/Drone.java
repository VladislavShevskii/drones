package com.vshevskii.dao.entities;

import com.vshevskii.dao.enums.ModelType;
import com.vshevskii.dao.enums.StateType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity()
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Drone {

    @Id
    @GeneratedValue
    private int id;

    @Column(length = 100)
    private String serialNumber;

    @Enumerated
    private ModelType model;

    private int weightLimit;

    private int battery;

    @Enumerated
    private StateType state;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "drone_to_medication",
            joinColumns = {@JoinColumn(name = "drone_id")},
            inverseJoinColumns = {@JoinColumn(name = "medication_id")})
    private List<Medication> medications;
}
