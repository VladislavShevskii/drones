package com.vshevskii.dao.enums;

public enum ModelType {
    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT
}
