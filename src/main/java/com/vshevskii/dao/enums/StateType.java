package com.vshevskii.dao.enums;

public enum StateType {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
