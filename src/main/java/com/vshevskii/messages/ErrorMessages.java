package com.vshevskii.messages;

public class ErrorMessages {

    public static final String DRONE_NOT_FOUND = "Drone with id '%s' not found";

    public static final String CONTAINS_VIOLATIONS = "Dto contains constraint violations";

    public static final String DRONE_HAS_NOT_COMPLETED_FLIGHT = "The drone with id '%s' hasn't completed a flight of " +
            "%d meters with a weight of %d grams, there was not enough battery";

    public static final String DRONE_HAS_NOT_RETURNED = "The drone with id '%s' hasn't returned";

    public static final String DRONE_INVALID_STATE = "The drone in state %s cannot be started";

    public static final String DRONE_INVALID_STATE_FOR_LOAD = "The drone in state %s cannot be loaded with items";

    public static final String DRONE_INVALID_STATE_FOR_COMPLETE_LOAD = "The drone in state %s cannot complete load";

    public static final String DRONE_INVALID_BATTERY_LEVEL_FOR_LOAD = "The drone with battery level %d%% cannot be " +
            "loaded";

    public static final String DRONE_INVALID_STATE_FOR_CHARGING = "The drone in state %s cannot be charged";

    public static final String DRONE_INVALID_BATTERY_LEVEL = "The drone with battery level %d%% cannot be started";

    public static final String DRONE_WEIGHT_LIMIT_EXCEEDED = "The drone contains medications of weight %d grams, " +
            "it cannot take more than its weight limit %d";
}
