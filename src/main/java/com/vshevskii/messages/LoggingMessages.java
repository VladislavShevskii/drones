package com.vshevskii.messages;

public class LoggingMessages {

    public static final String CHECKING_BATTERY = "Drone with id '%d' has battery %d%%";

    public static final String LOGGING_SPLITTER = "--------------------------------------------------------\n";
}
